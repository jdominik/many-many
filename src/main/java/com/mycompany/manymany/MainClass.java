/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.manymany;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author RENT
 */
public class MainClass {
    public static void main(String args[]) {
        

        SessionFactory instance = ConfigHibernate.getInstance();
        Session openSession = instance.openSession();
        Transaction beginTransaction = openSession.beginTransaction();
        Ship ship = new Ship();
        ship.setType("transatlantic");
        ship.setName("Victoria");
        ship.setCapacity(1000);
        openSession.save(ship);
    }
    
}
