/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.manymany;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

/**
 *
 * @author RENT
 */
@Entity
public class Course implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    
    @Column
    private String type;
    @Column
    private BigDecimal basicPrice;
    @Column
    private Date startDate;
    @Column
    private String name;
    @Column
    private Integer tripDays;
    
    @JoinColumn(name = "id_ship", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Ship idShip;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idCourse")
    private Collection<Reservation> reservationsCollection;

    public void setIdShip(Ship idShip) {
        this.idShip = idShip;
    }

    public void setReservationsCollection(Collection<Reservation> reservationsCollection) {
        this.reservationsCollection = reservationsCollection;
    }

    public Ship getIdShip() {
        return idShip;
    }

    public Collection<Reservation> getReservationsCollection() {
        return reservationsCollection;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setBasicPrice(BigDecimal basicPrice) {
        this.basicPrice = basicPrice;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setTripDays(Integer tripDays) {
        this.tripDays = tripDays;
    }

    public String getType() {
        return type;
    }

    public BigDecimal getBasicPrice() {
        return basicPrice;
    }

    public Date getStartDate() {
        return startDate;
    }

    public String getName() {
        return name;
    }

    public Integer getTripDays() {
        return tripDays;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Course)) {
            return false;
        }
        Course other = (Course) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.manymany.Course[ id=" + id + " ]";
    }
    
}
