/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.manymany;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

/**
 *
 * @author RENT
 */
@Entity
public class Reservation implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    
    @Column
    private Date reservationdate;
    @Column
    private BigDecimal price;
    @Column
    private Integer id_course;
   
    @ManyToMany
    @JoinTable(name="ReservationCustomer", joinColumns={@JoinColumn(name="id_reservation")},
            inverseJoinColumns={@JoinColumn(name="id_customer")})
    private Set<Customer> customers = new HashSet<Customer>();
    public Set<Customer> getCustomers() {
        return customers;
    }
    
    @JoinColumn(name = "id_course", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Course idCourse;

    public void setReservationdate(Date reservationdate) {
        this.reservationdate = reservationdate;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public void setId_course(Integer id_course) {
        this.id_course = id_course;
    }

    public Date getReservationdate() {
        return reservationdate;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public Integer getId_course() {
        return id_course;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Reservation)) {
            return false;
        }
        Reservation other = (Reservation) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.manymany.Reservation[ id=" + id + " ]";
    }
    
}
